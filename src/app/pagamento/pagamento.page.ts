import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BolosService} from '../servicos/bolos.service';
import {Bolo} from '../modelos/bolo';
import {LoadingController, NavController, ToastController} from '@ionic/angular';

@Component({
  selector: 'app-pagamento',
  templateUrl: './pagamento.page.html',
  styleUrls: ['./pagamento.page.scss'],
})
export class PagamentoPage implements OnInit {

  bolo: any = {};
  id: number;
  loading: any;

  constructor(
      private loadingController: LoadingController,
      private toastController: ToastController,
      private activatedRoute: ActivatedRoute,
      private boloService: BolosService,
      private navCtrl: NavController
  ) {
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit() {
    this.getBolo();
  }

  async confirmar() {
    await this.presentLoading();
    await this.navCtrl.navigateRoot('home').then(()=>{
      this.loading.dismiss();
      this.presentToast('Seu pedido ja vai chegar!!!');
    })
   
  }

  getBolo() {
    this.bolo = this.boloService.getBolos().subscribe(data => {
      this.bolo = data[this.id - 1];
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Por favor, aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

}
