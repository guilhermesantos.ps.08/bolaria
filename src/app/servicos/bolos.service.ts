import { Injectable } from '@angular/core';
import {Bolo} from '../modelos/bolo';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BolosService {
  constructor(private client: HttpClient) { }
  getBolos() {
    return this.client.get('http://localhost:80/bolaria/bolos.php');
  }
}
