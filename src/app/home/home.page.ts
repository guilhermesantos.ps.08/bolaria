import {Component, OnInit} from '@angular/core';
import {Bolo} from '../modelos/bolo';
import {BolosService} from '../servicos/bolos.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  bolos: any = [];

  constructor(private boloService: BolosService) {}

  getBolos() {
    this.boloService.getBolos().subscribe(data => {
      this.bolos = data;
      console.log(this.bolos);
    });
  }

  ngOnInit() {
      this.getBolos();
  }

}
