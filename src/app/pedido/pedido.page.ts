import { Component, OnInit } from '@angular/core';
import {Bolo} from '../modelos/bolo';
import {ActivatedRoute} from '@angular/router';
import {BolosService} from '../servicos/bolos.service';
import {LoadingController, ToastController, NavController} from '@ionic/angular';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.page.html',
  styleUrls: ['./pedido.page.scss'],
})
export class PedidoPage implements OnInit {

  bolo: Bolo = {};
  id: number;
  loading: any;

  constructor(
      private loadingController: LoadingController,
      private toastController: ToastController,
      private activatedRoute: ActivatedRoute,
      private boloService: BolosService,
      private navCtrl: NavController
  ) {
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit() {
    this.getBolo();
  }

  getBolo() {
    this.boloService.getBolos().subscribe(data => {
      this.bolo = data[this.id - 1];
    });
  }

  async pagar(id: string) {
    await this.presentLoading();
    this.navCtrl.navigateRoot(`pagamento/${id}`).then(()=>{
      this.loading.dismiss();
    })
  }


  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Por favor, aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }




}
